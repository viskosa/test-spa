import { call, put, takeEvery } from 'redux-saga/effects'
import { hideLoader, showLoader, setAuthors } from '../actions/authors.action'
import { REQUEST_AUTHORS } from '../constants/authors.const'

export function* sagaWatcher() {
    yield takeEvery(REQUEST_AUTHORS, sagaWorker)
}

function* sagaWorker() {
    try {
        yield put(showLoader())
        const payload = yield call(getAuthors)
        yield put(setAuthors(payload))
        yield put(hideLoader())
    } catch (err) {
        console.error('Something went wrong: ', err)
        yield put(hideLoader())
    }
}

async function getAuthors() {
    const fakeServer = () => {
        return new Promise(resolve => {
            setTimeout(() => {
                resolve(
                    [{
                        "name": "Kaylie", 
                        "surname": "Minoug"
                    },
                    {
                        "name": "Witnie", 
                        "surname": "Huston"
                    }]
                )
            }, 2000)
        }) 
    }

    return await fakeServer()
}