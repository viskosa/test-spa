// 1. Создать Single Page Application на React-e с использованием TypeScript
// 2. есть сервер get entry point /authors
// который будет возвращать json вида [ {"name":"Kaylie, "surname":"Minoug"}, {"name":"Witnie, "surname":"Huston"} ....]
// 2. Отобразить их на странице используя замоканый респонс сервера
// 3. Подключить Testing Library
// 4. Покрыть тестами.

import React from 'react';
import ReactDOM from 'react-dom';
import reportWebVitals from './reportWebVitals';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware, compose } from 'redux';

// saga
import createSagaMiddleware from 'redux-saga'
import { sagaWatcher } from './sagas/sagas';

// reducers
import rootReducer from './reducers/root.rd.js';

// components
import App from './App';

const saga = createSagaMiddleware();

// store
const enhancer = compose(
        applyMiddleware(saga)
    );
const initialState = {};
const store = createStore(rootReducer, initialState, enhancer);

saga.run(sagaWatcher)

ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById('root')
);

reportWebVitals();
