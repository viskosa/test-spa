import {
    SHOW_LOADER,
    HIDE_LOADER,
    REQUEST_AUTHORS,
    SET_AUTHORS
} from '../constants/authors.const.js';

export function requestAuthors() {
    return {
        type: REQUEST_AUTHORS
    }
}

export function setAuthors(payload) {
    return {
        type: SET_AUTHORS,
        payload
    }
}

export function showLoader() {
    return {
        type: SHOW_LOADER
    }
}

export function hideLoader() {
    return {
        type: HIDE_LOADER
    }
}