import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

// Actions
import * as authorsActions from '../actions/authors.action';

// Components
import AuthorsList from '../components/AuthorsList'
import { Preloader } from '../components/Preloader'

// styles
import '../styles/Authors.css';

class Authors extends React.Component {
    componentDidMount() {
        const { actions: { requestAuthors } } = this.props;
        requestAuthors();
    }

    render() {
        const {
            authors = [],
            fetch = false
        } = this.props;

        return (
            <div className="authors">
                <h1 className="authors__title">Authors</h1>

                { 
                    fetch 
                    ? <Preloader /> 
                    : authors && authors.length 
                    ? <AuthorsList authors={authors} />
                    : <p>Sorry, there are no authors for now!</p>
                }
            </div>
        )
    }
}

const mapStateToProps = state => ({
    ...state.authors
})

const mapDispatchToProps = dispatch => {
	return {
		actions: bindActionCreators({
			...authorsActions
		}, dispatch),
		dispatch
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(Authors);