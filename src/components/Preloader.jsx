import React from 'react'

// styles
import '../styles/Preloader.css';

export const Preloader = () => {
	return (
        <div className="preloader fl fl-align-c fl-justify-c">
            <div className="preloader__list">
                <span className="preloader__item preloader__item--1"></span>
                <span className="preloader__item preloader__item--2"></span>
                <span className="preloader__item preloader__item--3"></span>
            </div>
        </div>
    );
}
