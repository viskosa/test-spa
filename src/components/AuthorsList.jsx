import React from 'react'

export default function AuthorsList({ authors }) {
    return (
        <ul className="authors__list">
            {
                authors.map(({name = '', surname = ''}) => {
                    return (
                        <li key={name} className="authors__item">
                            <p>Name: {name}</p>
                            <p>Surmane: {surname}</p>
                        </li>
                    )
                })
            }
        </ul>
    )
}