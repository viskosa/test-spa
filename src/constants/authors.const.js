export const SHOW_LOADER = 'SHOW_LOADER';
export const HIDE_LOADER = 'HIDE_LOADER';
export const REQUEST_AUTHORS = 'REQUEST_AUTHORS';
export const SET_AUTHORS = 'SET_AUTHORS';
