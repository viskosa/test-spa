import { combineReducers } from 'redux'

// reducers
import authors from './authors.rd.js' 

const rootReducer = combineReducers({
    authors
})

export default rootReducer;