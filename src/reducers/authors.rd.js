import {
    SET_AUTHORS,
    SHOW_LOADER,
    HIDE_LOADER
} from '../constants/authors.const.js';

const initialState = {};

export default function authors(state = initialState, action) {
    switch(action.type) {
        case SHOW_LOADER: 
            return {
                ...state,
                fetch: true
            }

        case HIDE_LOADER: 
            return {
                ...state,
                fetch: false
            }

        case SET_AUTHORS:
            return {
                ...state,
                authors: action.payload
            }

        default: 
            return { ...state }
    }
}