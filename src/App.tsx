import React from 'react';

// components
import Authors from './containers/Authors'

function App() {
    return (
        <div className="App">
            <Authors />
        </div>
    );
}

export default App;
